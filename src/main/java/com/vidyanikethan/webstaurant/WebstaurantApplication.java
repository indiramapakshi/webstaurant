package com.vidyanikethan.webstaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages="com.vidyanikethan.webstaurant")
public class WebstaurantApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebstaurantApplication.class, args);
	}
}
