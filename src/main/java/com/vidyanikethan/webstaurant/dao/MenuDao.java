package com.vidyanikethan.webstaurant.dao;

import java.util.List;

import com.vidyanikethan.webstaurant.entity.Menu;

public interface MenuDao {
	public void createMenu(Menu menu);

	public Menu getMenu(String key);

	public List<Menu> getMenuList();

	public Menu update(Menu menu);

	public void delete(Menu menu);
}
