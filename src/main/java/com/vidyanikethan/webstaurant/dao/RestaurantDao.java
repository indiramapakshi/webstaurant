package com.vidyanikethan.webstaurant.dao;

import java.util.List;

import com.vidyanikethan.webstaurant.entity.Restaurant;

public interface RestaurantDao {

	public Restaurant create(Restaurant restaurant);
	
	public Restaurant getRestaurant(String restaurantId);
	
	public List<Restaurant> getRestaurants(List<Restaurant> restaurantIds);
	
	public List<Restaurant> getRestaurants();
	
}
