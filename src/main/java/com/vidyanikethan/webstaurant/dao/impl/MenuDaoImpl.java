package com.vidyanikethan.webstaurant.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vidyanikethan.webstaurant.dao.MenuDao;
import com.vidyanikethan.webstaurant.entity.Menu;
import com.vidyanikethan.webstaurant.repository.CassandraRepository;

@Repository
public class MenuDaoImpl implements MenuDao {

	@Autowired
	CassandraRepository cassandraRepository;

	@Override
	public void createMenu(Menu menu) {
		// TODO Auto-generated method stub
		cassandraRepository.create(menu);
	}

	@Override
	public Menu getMenu(String key) {
		// TODO Auto-generated method stub
		return (Menu) cassandraRepository.findById(key, Menu.class);
	}

	@Override
	public List<Menu> getMenuList() {
		// TODO Auto-generated method stub
		return cassandraRepository.selectAll(Menu.class);
	}

	@Override
	public Menu update(Menu menu) {
		// TODO Auto-generated method stub
		return cassandraRepository.update(menu);
	}

	@Override
	public void delete(Menu menu) {
		// TODO Auto-generated method stub
		cassandraRepository.delete(menu);
	}

}
