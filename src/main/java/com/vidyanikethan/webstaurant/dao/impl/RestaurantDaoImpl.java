package com.vidyanikethan.webstaurant.dao.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vidyanikethan.webstaurant.dao.RestaurantDao;
import com.vidyanikethan.webstaurant.entity.Restaurant;
import com.vidyanikethan.webstaurant.repository.CassandraRepository;

@Repository
public class RestaurantDaoImpl extends CassandraRepository implements RestaurantDao {

	@Autowired
	CassandraRepository cassandraRepository;
	
	@Override
	public Restaurant create(Restaurant restaurant) {
		// TODO Auto-generated method stub
		restaurant.setKey(UUID.randomUUID().toString());
		cassandraRepository.create(restaurant);
		return restaurant;
	}

	@Override
	public Restaurant getRestaurant(String restaurantId) {
		// TODO Auto-generated method stub
		return cassandraRepository.findById(restaurantId, Restaurant.class);
	}

	@Override
	public List<Restaurant> getRestaurants(List<Restaurant> restaurantIds) {
		// TODO Auto-generated method stub
		return cassandraRepository.selectByIds(restaurantIds, Restaurant.class);
	}
	
	@Override
	public List<Restaurant> getRestaurants() {
		// TODO Auto-generated method stub
		return cassandraRepository.selectAll(Restaurant.class);
	}

}
