package com.vidyanikethan.webstaurant.service;

import java.util.List;

import com.vidyanikethan.webstaurant.entity.Menu;

public interface MenuService {

	public Menu createMenu(Menu menu);

	public Menu getMenu(String menuId);

	public Menu updateMenu(Menu menu);

	public List<Menu> getMenus();
}
