package com.vidyanikethan.webstaurant.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidyanikethan.webstaurant.dao.MenuDao;
import com.vidyanikethan.webstaurant.entity.Menu;
import com.vidyanikethan.webstaurant.service.MenuService;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDao menuDao;

	@Override
	public Menu createMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.setKey(UUID.randomUUID().toString());
		menuDao.createMenu(menu);
		return menu;
	}

	@Override
	public Menu getMenu(String menuId) {
		// TODO Auto-generated method stub
		return menuDao.getMenu(menuId);
	}

	@Override
	public Menu updateMenu(Menu menu) {
		// TODO Auto-generated method stub
		return menuDao.update(menu);
	}

	@Override
	public List<Menu> getMenus() {
		// TODO Auto-generated method stub
		return menuDao.getMenuList();
	}

}
