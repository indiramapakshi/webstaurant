package com.vidyanikethan.webstaurant.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidyanikethan.webstaurant.dao.RestaurantDao;
import com.vidyanikethan.webstaurant.entity.Restaurant;
import com.vidyanikethan.webstaurant.service.RestaurantService;

@Service
public class RestaurantServiceImpl implements RestaurantService {

	@Autowired
	RestaurantDao restaurantDao;
	
	@Override
	public Restaurant createRestaurant(Restaurant restaurant) {
		// TODO Auto-generated method stub
		return restaurantDao.create(restaurant);
	}

	@Override
	public Restaurant getRestaurant(String id) {
		// TODO Auto-generated method stub
		return restaurantDao.getRestaurant(id);
	}

	@Override
	public List<Restaurant> getRestaurants(List<Restaurant> restaurants) {
		// TODO Auto-generated method stub
		return restaurantDao.getRestaurants(restaurants);
	}

	@Override
	public List<Restaurant> getRestaurants() {
		// TODO Auto-generated method stub
		return restaurantDao.getRestaurants();
	}
	

}
