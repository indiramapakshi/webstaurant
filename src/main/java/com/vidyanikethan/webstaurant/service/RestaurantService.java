package com.vidyanikethan.webstaurant.service;

import java.util.List;

import com.vidyanikethan.webstaurant.entity.Restaurant;

public interface RestaurantService {

	public Restaurant createRestaurant(Restaurant restaurant);
	
	public Restaurant getRestaurant(String id);
	
	public List<Restaurant> getRestaurants(List<Restaurant> restaurants);
	
	public List<Restaurant> getRestaurants();
}
