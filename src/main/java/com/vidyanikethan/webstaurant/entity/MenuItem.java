package com.vidyanikethan.webstaurant.entity;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table("menuitem")
public class MenuItem {
	
	@PrimaryKey
	@Column("key")
	private String restaurantId;
	
	@Column("column1")
	private String itemId;
	
	@Column("column2")
	private String category;
	
	@Column("type")
	private String type;

	@Column("value")
	private String value;
	
	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
