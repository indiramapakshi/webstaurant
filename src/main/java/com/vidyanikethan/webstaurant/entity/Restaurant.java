package com.vidyanikethan.webstaurant.entity;

import java.util.List;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table("restaurant")
public class Restaurant {

	@PrimaryKey("key")
	private String key;

	@Column("name")
	private String name;

	@Column("location")
	private String location;

	@Column("address")
	private String address;

	@Column("state")
	private String state;
	
	@Column("city")
	private String city;

	@Column("pincode")
	private String pindcode;

	@Column("country")
	private String country;

	@Column("cuisineId")
	private String cuisineId;
	
	@Column("contactno")
	private String contactNumber;
	
	@Column("avgRating")
	private double avgRating;

	@Column("type")
	private String type;
	
	private List<Menu> menus;
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPindcode() {
		return pindcode;
	}

	public void setPindcode(String pindcode) {
		this.pindcode = pindcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCuisineId() {
		return cuisineId;
	}

	public void setCuisineId(String cuisineId) {
		this.cuisineId = cuisineId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
