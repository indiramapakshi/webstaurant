package com.vidyanikethan.webstaurant.entity;

public enum Cuisine {
	Indian, Mexican, Italian, Chinese
}
