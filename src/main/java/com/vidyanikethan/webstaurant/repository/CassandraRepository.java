package com.vidyanikethan.webstaurant.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.stereotype.Repository;

import com.vidyanikethan.webstaurant.entity.Restaurant;

@Repository
public class CassandraRepository {

	public CassandraRepository() {
		System.out.println(" CassandraRepository() ");
	}

	@Autowired
	public CassandraOperations cassandraTemplate;

	public <T> void create(T entity) {
		cassandraTemplate.insert(entity);
	}

	public <T> T update(T entity) {
		return (T) cassandraTemplate.update(entity);
	}

	public <T> void delete(T entity) {
		cassandraTemplate.delete(entity);
	}

	public <T> T findById(Object id, Class<T> clazz) {
		return cassandraTemplate.selectOneById(clazz, id);
	}

	public <T> List<T> selectByIds(List<T> ids, Class<T> clazz) {
		return (List<T>) cassandraTemplate.selectBySimpleIds(clazz, ids);
	}

	public <T> List<T> selectAll(Class<T> clazz) {
		return (List<T>) cassandraTemplate.selectAll(clazz);
	}
}
