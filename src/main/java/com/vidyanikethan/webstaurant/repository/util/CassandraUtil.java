package com.vidyanikethan.webstaurant.repository.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.convert.CassandraConverter;
import org.springframework.data.cassandra.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;

@PropertySource(value = {"classpath:application.properties"})
public class CassandraUtil {

	@Autowired
	public static Environment environment;

	public CassandraUtil() {
		System.out.println("Cassandra Utils");
	}

	private static String getKeyspaceName() {
		return environment.getProperty("spring.data.cassandra.keyspace-name");
	}

	private static String getConnectionPoint() {
		return environment.getProperty("spring.data.cassandra.contact-points");
	}

	private static int getPort() {
		return Integer.parseInt(environment.getProperty("spring.data.cassandra.port"));
	}

	@Bean
	public CassandraConverter converter() {
		return new MappingCassandraConverter(mappingContext());

	}

	@Bean
	public BasicCassandraMappingContext mappingContext() {
		return new BasicCassandraMappingContext();

	}

	@Bean
	public CassandraClusterFactoryBean cluster() {
		CassandraClusterFactoryBean cassandraClusterFactoryBean = new CassandraClusterFactoryBean();
		cassandraClusterFactoryBean.setContactPoints(getConnectionPoint());
		cassandraClusterFactoryBean.setPort(getPort());
		return cassandraClusterFactoryBean;
	}

	@Bean
	public CassandraSessionFactoryBean session() {
		CassandraSessionFactoryBean cassandraSessionFactoryBean = new CassandraSessionFactoryBean();
		cassandraSessionFactoryBean.setCluster(cluster().getObject());
		cassandraSessionFactoryBean.setKeyspaceName(getKeyspaceName());
		cassandraSessionFactoryBean.setConverter(converter());
		cassandraSessionFactoryBean.setSchemaAction(SchemaAction.CREATE_IF_NOT_EXISTS);
		return cassandraSessionFactoryBean;
	}

	@Bean
	public CassandraOperations cassandraTemaplate() {
		return new CassandraTemplate(session().getObject());

	}

}
