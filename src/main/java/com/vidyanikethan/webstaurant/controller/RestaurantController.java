package com.vidyanikethan.webstaurant.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vidyanikethan.webstaurant.entity.Restaurant;
import com.vidyanikethan.webstaurant.service.RestaurantService;

@RestController
@RequestMapping("/webstaurant")
public class RestaurantController {

	@Autowired
	RestaurantService restaurantService;

	@GetMapping("/echo")
	public String echo(){
		return "echo";
	}
	
	@RequestMapping(value = "/restaurant", method = RequestMethod.POST)
	public Restaurant createRestaurant(@RequestBody Restaurant restaurant) {
		return restaurantService.createRestaurant(restaurant);
	}

	@RequestMapping(value = "/restaurant/{id}", method = RequestMethod.GET)
	public Restaurant getRestaurant(@PathVariable("id") String restuarantId) {
		return restaurantService.getRestaurant(restuarantId);
	}

	@RequestMapping(value = "/restaurants/{ids}", method = RequestMethod.GET)
	public List<Restaurant> getRestaurantByIds(@PathVariable("ids") String[] restaurantIds) {
		if (null != restaurantIds) {
			List<Restaurant> restaurants = new ArrayList<Restaurant>(restaurantIds.length);
			Restaurant restaurant;
			for (String restaurantId : restaurantIds) {
				restaurant = new Restaurant();
				restaurant.setKey(restaurantId);
				restaurants.add(restaurant);
			}
			return restaurantService.getRestaurants(restaurants);
		}
		return null;
	}
	
	@GetMapping("/restaurants")
	public List<Restaurant> getRestaurants() {
		return restaurantService.getRestaurants();
	}
}
