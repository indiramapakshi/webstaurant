package com.vidyanikethan.webstaurant.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.AccessType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vidyanikethan.webstaurant.entity.Cuisine;
import com.vidyanikethan.webstaurant.entity.Menu;
import com.vidyanikethan.webstaurant.service.MenuService;

@RestController
@RequestMapping("/webstaurant")
public class MenuController {

	@Autowired
	MenuService menuService;
	
	@GetMapping("/cuisine")
	public List<Cuisine> getCuisine(){
		return Arrays.asList(Cuisine.values());
	}
	@PostMapping("/menu")
	public Menu createMenu(@RequestBody Menu menu) {
		menuService.createMenu(menu);
		return menu;
	}
	
	@GetMapping("/menu/{menuId}")
	public Menu getMenu(@PathVariable String menuId) {
		return menuService.getMenu(menuId);
	}
	
	@PutMapping("/menu")
	public Menu updateMenu(@RequestBody Menu menu) {
		return menuService.updateMenu(menu);
	}
	
	@GetMapping("/menu")
	public List<Menu> getMenus() {
		return menuService.getMenus();
	}
}
